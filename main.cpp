#include <iostream>
#include <string>
#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include "tree.h"

using namespace std;

int main(void){
	int num, randNum, del, deltimes, find, treeHeight, newTree;
	Tree<int> mytree(num), balanceTree;
	
	cout << "How many numbers do you want?: ";
	cin >> num;
	cout << endl;
	srand(time(0));
	for (int i = 0; i < num; i++) {
		randNum = (rand() % 100) + 1;
		cout << randNum;
		if(i-num != 1){
			cout << " ";	
		}
		mytree.insert(randNum);
	}
	cout << " are added into the tree." << endl << endl;
	//Show original height of the tree
	treeHeight = mytree.height();
	cout << "Current height: " << treeHeight << endl << endl;
	//Showing in order
	cout << "Showing in order: ";
	mytree.inorder();
	cout << endl << endl;
	//Balancing the tree
	cout << "Balanced: " ;	
	balanceTree.balance(mytree.arr_tree,0,num-1);
	cout << endl << endl;
	//Show new height
	newTree = balanceTree.height();
	cout << "New height: " << newTree << endl << endl;
	//Search
	cout << "Search the number: ";
	cin >> find;
	if (balanceTree.search(find)) {
		cout << find << " found" << endl << endl;
	}
	else{
		cout << find << " not found" << endl << endl;
	}
	//Try delete some numbers
	cout << "How many times do you want to attempt to delete?:  " ;
	cin >> deltimes;
	cout << "Results : " << endl;
	for (int i = 0; i < deltimes; i++){
		del = rand() % 100 + 1;
		if(mytree.remove(del) == 0){
			cout << i+1 << " - " << del << " >>> no matched element." << endl;
		}
		else{
			cout << i+1 << " - "<< del << " deleted." << endl;
		}		
	}
	//Final tree
	cout << endl << "Final tree: " ;	
	balanceTree.balance(mytree.arr_tree,0,num-1);
}
